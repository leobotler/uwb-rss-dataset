# README #

### What is this repository for? ###

This repository contains 3 different datasets of measurements using the dw1000 UWB transceiver using different transmission powers. 2 datasets were obtained in the same environment (hallway), of which one used the AGC of the receiver turned off while the other was obtained with the AGC turned on. The 3rd dataset was obtained in a different environment (hall) with the transceivers' AGC turned off.

### How to use the datasets? ###

Each dataset corresponds to a dataframe and is stored as a binary object.
The dataframe can be loaded as follows:
1. Using python version 3.9.12 and pandas version 1.4.1
2. Use the method read_pickle from pandas library, e.g., read_pickle("<dataset>.pickle")  

### Parameters of transceivers ###

Channel 7 (f=6489.6 MHz, Bw=1081.6 MHz) 
distance range in meters = {0.5, 1., ..., 6.5}
68 different powers, at least 16 transmissions each.

### Who do I talk to? ###

* Leo Botler
* leo.happbotler@tugraz.at or leobotler@gmail.com